package com.UHF.util;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DBDao {  
    DBOpenHelper dbOpenHelper;  
      
    public DBDao(Context context){  
        this.dbOpenHelper=new DBOpenHelper(context);  
    }  
    public void createtable(){  
    	SQLiteDatabase db=dbOpenHelper.getWritableDatabase();  
    	dbOpenHelper.onCreate(db);
        db.close();  
    }  
    /** 
     * 添加一条数据 
     * @param user 
     */  
    public void save(String url){  
        SQLiteDatabase db=dbOpenHelper.getWritableDatabase();  
        db.execSQL("insert into baseSet(url) values(?)", new Object[]{url});  
        db.close();  
    }  
    /** 
     * 更新一条数据 
     * @param user 
     */  
    public void update(String url){  
        SQLiteDatabase db=dbOpenHelper.getWritableDatabase();  
        db.execSQL("update baseSet set url=?", new Object[]{url});  
        db.close();  
    }  
    /** 
     * 查找当前系统url
     * @param uid 
     */  
    public String find(){  
        SQLiteDatabase db=dbOpenHelper.getReadableDatabase();  
        Cursor cursor =db.rawQuery("select * from baseSet ", null);  
        if(cursor.moveToFirst()){  
            String url=cursor.getString(cursor.getColumnIndex("url"));  
            return url;  
        }  
        cursor.close();  
        return null;  
    }  
    /** 
     * 获取数据总数 
     * @return 
     */  
    public long getCount(){  
        SQLiteDatabase db=dbOpenHelper.getReadableDatabase();  
        Cursor cursor =db.rawQuery("select count(*) from baseSet", null);  
        cursor.moveToFirst();  
        long reslut=cursor.getLong(0);  
        return reslut;  
    }  
}  
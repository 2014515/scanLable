package com.UHF.scanlable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ActivityGroup;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.UHF.R;
import com.UHF.model.AcceptanceAdapter;
import com.UHF.model.CheckBoxAdapter;
import com.UHF.turntable.CircleActivity;
import com.UHF.turntable.ScanActivity;
import com.UHF.util.CheckDataFromServer;

public class QueryActivity extends ScanActivity implements OnClickListener{

	Button refresh;
	Button query_bt;
	/**
	 * 单据号/状态
	 */
	private LinkedHashMap<Object,Object> listDetailData=new LinkedHashMap<Object,Object>();
	private Map<String,Integer> data=new HashMap<String, Integer>();
	private Map<String,Integer> oldData=new HashMap<String, Integer>();
	/**
	 * 新扫物料
	 */
	private List<String> materialAddData=new ArrayList<String>();
	/**
	 * 新扫描托盘
	 */
	private List<String> trayAddData=new ArrayList<String>();
	/**
	 * 新扫描库位
	 */
	private List<String> positionAddData=new ArrayList<String>();
	/**
	 * 新扫描产品
	 */
	private List<String> productAddData=new ArrayList<String>();
	/**
	 */
	private LinkedHashMap listData=new LinkedHashMap<Object,Object>();
	
	private AcceptanceAdapter myAdapter;
	private ListView listView;
	private Intent intent1;
	private Intent intent2;
	private Intent intent3;
	private Intent intent4;
	//标题
	private TextView ip_title;
	private Handler mHandler;
	private String lastBt="0";
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search);
		refresh = (Button)findViewById(R.id.query_refresh);
		refresh.setOnClickListener(this);
		query_bt = (Button)findViewById(R.id.query_bt);
		query_bt.setOnClickListener(this);
		
		listView = (ListView)findViewById(R.id.query_list);//
		intent1 = new Intent(this,MaterialInfoActivity.class);
		intent2 = new Intent(this,TrayInfoActivity.class);
		intent3 = new Intent(this,PositionInfoActivity.class);
		intent4 = new Intent(this,ProductInfoActivity.class);
		listView.setOnItemClickListener(new OnItemClickListener(){  
			  
            @Override  
            public void onItemClick(AdapterView<?> parent, View view,  
                    int position, long id) {  
        		String iNumber = myAdapter.getmList().get(position);
        		LinkedHashMap<Object,Object> m=(LinkedHashMap<Object, Object>) listDetailData.get(iNumber);
        		String types=(String)m.get("types");
        		Intent intentx = null;
        		//类型（1物料 2托盘3库位4成品）
        		if("1".equals(types)){
        			intentx=intent1;
        		}else if("2".equals(types)){
        			intentx=intent2;
        		}else if("3".equals(types)){
        			intentx=intent3;
        		}else if("4".equals(types)){
        			intentx=intent4;
        		}
        		intentx.putExtra("scanCode", (String)m.get("scanCode"));
//        		intent.putExtra(MainActivity.EXTRA_EPC, myAdapter.getmList().get(position));
        		startActivity(intentx);
        		finish();
            }  
        });  
		mHandler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
	    		super.handleMessage(msg);
				switch (msg.what) {
				case 0:
					if(isCanceled) return;
					break;
				case 1:
					if(isCanceled) return;
					data = UfhData.scanResult6c;
					if(data==null||data.isEmpty()){
						break;
					}
					List checkList=checkMapData(oldData,data);
					checkIt(checkList);
					break;
				default:
					break;
				}
				
			}
			
		};
		
	}
	protected List checkMapData(Map oldMap,Map newMap){
		List list=new ArrayList<String>();
		Set s=oldMap.keySet();
		for(Object key:newMap.keySet()){  
			if(!s.contains(key.toString())){
				list.add(key);
				switch ((Integer)newMap.get(key)) {
				case 1:
					materialAddData.add((String)key);
					break;
//				case 2:
//					trayAddData.add((String)key);
//					break;
//				case 3:
//					positionAddData.add((String)key);
//					break;
				case 4:
					productAddData.add((String)key);
					break;
				default:
					break;
				}
			}
		} 
		return list;
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==refresh.getId()){
			if("0".equals(lastBt)){
				myAdapter = null;
//				materialListData.clear();
			}else{
				Toast.makeText(QueryActivity.this,"请先停止扫码!",Toast.LENGTH_SHORT).show();
			}
		}else if(v.getId()==query_bt.getId()){
			if("0".equals(lastBt)){
				lastBt="1";
				query_bt.setText(R.string.stop);
				openRFIDScan(mHandler,UfhData.scanResult6c,1,null);
			}else{
				lastBt="0";
				query_bt.setText(R.string.scan);
				closeRFIDScan();
			}
		}
		
		
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, CircleActivity.class); 
        startActivity(intent);
        finish();
	}
	private boolean checkMaterial(Map<String,Integer> map1){
		boolean type=false;
		for(String key:map1.keySet()){  
			if(listData.get(key)==null){
				type=true;
			}
		}
		return type;
	}
	@Override
	protected void showScanCode() {
		// TODO Auto-generated method stub
		super.showScanCode();
		Toast.makeText(QueryActivity.this,getBarcodeStr(),Toast.LENGTH_SHORT).show();
		data.put(getBarcodeStr(), 1);
		List checkList=checkMapData1(oldData,data);
		checkIt(checkList);
	}
	protected List checkMapData1(Map oldMap,Map newMap){
		List list=new ArrayList<String>();
		Set s=oldMap.keySet();
		for(Object key:newMap.keySet()){  
			if(!s.contains(key.toString())){
				list.add(key);
				materialAddData.add((String)key);
				productAddData.add((String)key);
			}
		} 
		return list;
	}
	private void checkIt(List checkList){
		if(checkList.size()>0){
			oldData.putAll(data);
			if(materialAddData.size()>0){
				CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
				String s=StringUtils.join(materialAddData.toArray(), ",");
				Map m=new HashMap();
				m.put("scanCode", s);
				m.put("user_id", UfhData.getEmployBean().getId());
				checkDataFromServer.setOperType("1");
				checkDataFromServer.setData(m);
				checkDataFromServer.setIp(UfhData.getIP());
				checkDataFromServer.setmHandler(handler);
				checkDataFromServer.setWhat(1);
				checkDataFromServer.checkData();
				materialAddData.clear();
			}
			if(trayAddData.size()>0){
				CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
				String s=StringUtils.join(trayAddData.toArray(), ",");
				Map m=new HashMap();
				m.put("scanCode", s);
				m.put("user_id", UfhData.getEmployBean().getId());
				checkDataFromServer.setOperType("2");
				checkDataFromServer.setData(m);
				checkDataFromServer.setIp(UfhData.getIP());
				checkDataFromServer.setmHandler(handler);
				checkDataFromServer.setWhat(2);
				checkDataFromServer.checkData();
				trayAddData.clear();
			}	
			if(positionAddData.size()>0){
				CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
				String s=StringUtils.join(positionAddData.toArray(), ",");
				Map m=new HashMap();
				m.put("scanCode", s);
				m.put("user_id", UfhData.getEmployBean().getId());
				checkDataFromServer.setOperType("3");
				checkDataFromServer.setData(m);
				checkDataFromServer.setIp(UfhData.getIP());
				checkDataFromServer.setmHandler(handler);
				checkDataFromServer.setWhat(3);
				checkDataFromServer.checkData();
				positionAddData.clear();
			}
			if(productAddData.size()>0){
				CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
				String s=StringUtils.join(productAddData.toArray(), ",");
				Map m=new HashMap();
				m.put("scanCode", s);
				m.put("user_id", UfhData.getEmployBean().getId());
				checkDataFromServer.setOperType("38");
				checkDataFromServer.setData(m);
				checkDataFromServer.setIp(UfhData.getIP());
				checkDataFromServer.setmHandler(handler);
				checkDataFromServer.setWhat(4);
				checkDataFromServer.checkData();
				productAddData.clear();
			}
		}else {
			if(myAdapter == null){
				myAdapter = new AcceptanceAdapter(QueryActivity.this, listData);
				listView.setAdapter(myAdapter);
			}else{
				myAdapter.setData(listData) ;
			}
			myAdapter.notifyDataSetChanged();
		}
	}
	


	private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        		super.handleMessage(msg);
        		try {
					String s= (String) msg.obj;
					if(s==null||"-1".equals(s)){
						Toast.makeText(QueryActivity.this,"扫码有误!",Toast.LENGTH_SHORT).show();
					}else{
						JSONArray objList;
						try {
		    				JSONObject ss= new JSONObject(s);
		    				String operType=ss.getString("operType");
							objList = new JSONArray(ss.getString("list"));
							String status="";
							switch (msg.what) {
			    			case 1:
			    				for (int i = 0; i< objList.length(); i++) {
									LinkedHashMap<Object,Object> map=new LinkedHashMap<Object,Object>();
					                //循环遍历，依次取出JSONObject对象
					                //用getInt和getString方法取出对应键值
					                JSONObject obj = objList.getJSONObject(i);
					                /*acceptanceData.put(checkHex(Long.toHexString(Long.parseLong(obj.get("epcId").toString())).toUpperCase(),"0001"),obj.get("packageInventoryNum"));//epc,流水号
					                String backGroundColor=obj.get("backGroundColor").equals("0")?"已复核":"未复核";
					                acceptanceCheckData.put(obj.get("packageInventoryNum").toString(),backGroundColor);//epc,流水号*/
					                try {
					                	map.put("scanCode", obj.get("scanCode"));
					                	map.put("types", "1");
						                listDetailData.put(obj.get("packageInventoryNum"), map);
//						                materialListData.put(obj.getString("scanCode"),obj.getString("packageInventoryNum"));
						                listData.put(obj.getString("packageInventoryNum"), "物料");
									} catch (Exception e) {
										// TODO: handle exception
									}
					             }
			    				break;
			    			case 2:
			    				for (int i = 0; i< objList.length(); i++) {
									LinkedHashMap<Object,Object> map=new LinkedHashMap<Object,Object>();
					                //循环遍历，依次取出JSONObject对象
					                //用getInt和getString方法取出对应键值
					                JSONObject obj = objList.getJSONObject(i);
					                /*acceptanceData.put(checkHex(Long.toHexString(Long.parseLong(obj.get("epcId").toString())).toUpperCase(),"0001"),obj.get("packageInventoryNum"));//epc,流水号
					                String backGroundColor=obj.get("backGroundColor").equals("0")?"已复核":"未复核";
					                acceptanceCheckData.put(obj.get("packageInventoryNum").toString(),backGroundColor);//epc,流水号*/
					                try {
					                	map.put("scanCode", obj.get("scanCode"));
					                	map.put("types", "2");
						                listDetailData.put(obj.get("code"), map);
//						                materialListData.put(obj.getString("scanCode"),obj.getString("packageInventoryNum"));
						                listData.put(obj.getString("code"), "托盘");
									} catch (Exception e) {
										// TODO: handle exception
									}
					             }
			    				break;
			    			case 3:
			    				for (int i = 0; i< objList.length(); i++) {
									LinkedHashMap<Object,Object> map=new LinkedHashMap<Object,Object>();
					                //循环遍历，依次取出JSONObject对象
					                //用getInt和getString方法取出对应键值
					                JSONObject obj = objList.getJSONObject(i);
					                /*acceptanceData.put(checkHex(Long.toHexString(Long.parseLong(obj.get("epcId").toString())).toUpperCase(),"0001"),obj.get("packageInventoryNum"));//epc,流水号
					                String backGroundColor=obj.get("backGroundColor").equals("0")?"已复核":"未复核";
					                acceptanceCheckData.put(obj.get("packageInventoryNum").toString(),backGroundColor);//epc,流水号*/
					                try {
					                	map.put("scanCode", obj.get("scanCode"));
					                	map.put("types", "3");
						                listDetailData.put(obj.get("positionCode"), map);
//						                materialListData.put(obj.getString("scanCode"),obj.getString("packageInventoryNum"));
						                listData.put(obj.getString("positionCode"), "库位");
									} catch (Exception e) {
										// TODO: handle exception
									}
					             }
			    				break;
			    			case 4:
			    				for (int i = 0; i< objList.length(); i++) {
									LinkedHashMap<Object,Object> map=new LinkedHashMap<Object,Object>();
					                //循环遍历，依次取出JSONObject对象
					                //用getInt和getString方法取出对应键值
					                JSONObject obj = objList.getJSONObject(i);
					                /*acceptanceData.put(checkHex(Long.toHexString(Long.parseLong(obj.get("epcId").toString())).toUpperCase(),"0001"),obj.get("packageInventoryNum"));//epc,流水号
					                String backGroundColor=obj.get("backGroundColor").equals("0")?"已复核":"未复核";
					                acceptanceCheckData.put(obj.get("packageInventoryNum").toString(),backGroundColor);//epc,流水号*/
					                try {
					                	map.put("scanCode", obj.get("scanCode"));
					                	map.put("types", "4");
						                listDetailData.put(obj.get("packageInventoryNum"), map);
//						                materialListData.put(obj.getString("scanCode"),obj.getString("packageInventoryNum"));
						                listData.put(obj.getString("packageInventoryNum"), "成品");
									} catch (Exception e) {
										// TODO: handle exception
									}
					             }
			    				break;
			    			default:
			    				break;
			    			}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if(myAdapter == null){
							myAdapter = new AcceptanceAdapter(QueryActivity.this, listData);
							listView.setAdapter(myAdapter);
						}else{
							myAdapter.setData(listData) ;
						}
						myAdapter.notifyDataSetChanged();
					}
				} catch (Exception e) {
					// TODO: handle exception
					Toast.makeText(QueryActivity.this,"批次号有误!",Toast.LENGTH_SHORT).show();
				}
            }

    };
}

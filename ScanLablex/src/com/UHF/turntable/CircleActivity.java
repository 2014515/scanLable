package com.UHF.turntable;


import com.UHF.R;
import com.UHF.scanlable.AcceptanceActivity;
import com.UHF.scanlable.AcceptanceListActivity;
import com.UHF.scanlable.ChangePositionActivity;
import com.UHF.scanlable.InventoryListActivity;
import com.UHF.scanlable.LoginActivity;
import com.UHF.scanlable.MaterialListActivity;
import com.UHF.scanlable.MaterialOutListActivity;
import com.UHF.scanlable.ProductListActivity;
import com.UHF.scanlable.ProductOutListActivity;
import com.UHF.scanlable.QueryActivity;
import com.UHF.scanlable.ScanMode;
import com.UHF.util.NormalDialog;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

/**
 *
 */
public class CircleActivity extends Activity
{

	private CircleMenuLayout mCircleMenuLayout;
	public static final String EXTRA_MODE = "mode";
	private String[] mItemTexts = new String[] { "查询 ", "换库位", "物料入库",
			"物料出库", "接受复核", "成品入库", "成品出库", "盘点" };
//	private String[] mItemTexts = new String[] { "物料入库",
//			"物料出库", "接受复核", "成品入库", "成品出库"};
	private int[] mItemImgs = new int[] { R.drawable.query,
			R.drawable.change_position, R.drawable.m_incoming,
			R.drawable.m_out, R.drawable.receive,
			R.drawable.p_incoming,R.drawable.p_out,
			R.drawable.inventory };
//	private int[] mItemImgs = new int[] { R.drawable.home_mbank_3_normal,
//			R.drawable.home_mbank_4_normal, R.drawable.home_mbank_5_normal,
//			R.drawable.home_mbank_6_normal,R.drawable.home_mbank_6_normal};

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		//自已切换布局文件看效果
		setContentView(R.layout.activity_main02);
//		setContentView(R.layout.activity_main);

		mCircleMenuLayout = (CircleMenuLayout) findViewById(R.id.id_menulayout);
		mCircleMenuLayout.setMenuItemIconsAndTexts(mItemImgs, mItemTexts);
		
		

		mCircleMenuLayout.setOnMenuItemClickListener(new CircleMenuLayout.OnMenuItemClickListener()
		{
			
			@Override
			public void itemClick(View view, int pos)
			{
				Toast.makeText(CircleActivity.this, mItemTexts[pos],
						Toast.LENGTH_SHORT).show();
				Intent intent = new Intent();
				switch (pos) {
				case 0://查询
					intent.putExtra(EXTRA_MODE, "search");
					intent.setClass(CircleActivity.this, QueryActivity.class);
					startActivity(intent);
			        finish();
					break;
				case 1://换库位
					intent.putExtra(EXTRA_MODE, "changePoition");
					intent.setClass(CircleActivity.this, ChangePositionActivity.class);
					startActivity(intent);
			        finish();
					break;
//				case 0://物料入库
				case 2://物料入库
					intent.putExtra(EXTRA_MODE, "Stock-In");
					intent.setClass(CircleActivity.this, MaterialListActivity.class);
					startActivity(intent);
			        finish();
					break;
//				case 1://物料出库
				case 3://物料出库
					intent.putExtra(EXTRA_MODE, "Stock-Out");
					intent.setClass(CircleActivity.this, MaterialOutListActivity.class);
					startActivity(intent);
			        finish();
					break;
//				case 2://接受复核
				case 4://接受复核
					intent.putExtra(EXTRA_MODE, "Material-Accept");
					intent.setClass(CircleActivity.this, AcceptanceListActivity.class);
					startActivity(intent);
			        finish();
					break;
//				case 3://成品入库
				case 5://成品入库
					intent.putExtra(EXTRA_MODE, "Product-Stock-In");
					intent.setClass(CircleActivity.this, ProductListActivity.class);
					startActivity(intent);
			        finish();
					break;
//				case 4://成品出库
				case 6://成品出库
					intent.putExtra(EXTRA_MODE, "Product-Stock-Out");
					intent.setClass(CircleActivity.this, ProductOutListActivity.class);
					startActivity(intent);
			        finish();
					
					break;
				case 7://盘点
					intent.putExtra(EXTRA_MODE, "Inventory");
					intent.setClass(CircleActivity.this, InventoryListActivity.class);
					startActivity(intent);
			        finish();
					break;
				default:
					break;
				}
			}
			
			@Override
			public void itemCenterClick(View view){
				//退出
//				Toast.makeText(CircleActivity.this,
//						"you can do something just like ccb  ",
//						Toast.LENGTH_SHORT).show();

                NormalDialog normalDialog = new NormalDialog(CircleActivity.this);
                normalDialog.setTitle("确定退出系统？");
                normalDialog.dialog.show();

                //重定义下确定的文本
                normalDialog.ok.setText("退出");
                //确认按键回调，按下确认后在此做处理
                normalDialog.setMyDialogOnClick(new NormalDialog.MyDialogOnClick() {
                    @Override
                    public void ok() {
                        Toast.makeText(CircleActivity.this, "退出成功", Toast.LENGTH_SHORT).show();
        				Intent intent = new Intent();
        				intent.setClass(CircleActivity.this, LoginActivity.class);
        				startActivity(intent);
        				finish();
                    }
                });
				
			}
		});
		
	}

}
